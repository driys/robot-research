Feature: Login Evermos

Background:
    Given User has opened the browser

  @tag
  Scenario Outline: Login using credential 
    Given User go to Evermos Login Page
    When User input phone number <Phone>
    And User input password <Password>
    And User click button Login
    Then User successfully logged in
    And User redirected to Dashboard page 

    @valid
    Examples: Valid Data
      | Phone    	     | Password | 
      | 6281223334444  | password | 

    @invalid
    Examples: Invalid Data
      | Phone    	     | Password | 
      | 6281223334444  | testing  |
      | 6281234567890  | testing  |
      | 6281223334444  |          |
      |                | password |
      |                |          |