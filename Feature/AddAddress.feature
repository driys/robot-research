Feature: Add Address Evermos

Background:
    Given User Already Login

  Scenario: Add Address with valid data
    Given User click menu Akun
    And User click menu Daftar Alamat Pribadi
    And User click button Tambah Alamat Baru
    When User input Judul Alamat
    And User input Nama Penerima
    And User input Nomor Telepon
    And User select Alamat
    And User input Detail Alamat
    And User click button Simpan
    And User click button OK on Browser Popup
    And User click button OK on Information Popup
    Then Add Address should be successfully
    And User redirected to List Address Page
    And New Address should be shown on List Address Page
