Feature: Login Arttha

Background:
    Given User has opened the browser

  Scenario: Login with valid credential 
    Given User go to Arttha Login Page
    When User input username
    And User input password
    And User click button Login
    Then User successfully logged in
    And User redirected to Dashboard page 
