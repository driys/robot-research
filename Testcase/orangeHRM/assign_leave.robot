*** Settings ***
Metadata          Feature    Assign Leave
Metadata          Generated by    _gherkin2robotframework on 2022-03-22T17:07:16.066788_
Library           SeleniumLibrary
Resource          ../step_definition/orangeHRM/assign_leave_step_definitions.robot
Resource          ../keywords/OrangeHRMKeywords.robot
Resource          ../variables/variables.robot

*** Test Cases ***
User assign leave using valid input
    Background
    Given User Click Menu Leave
    And User Click Assign Leave
    When User input Employee Name
    And User select Leave Type
    And User select From Date
    And User select To Date
    And User input Comment
    And User Click button Assign
    Then User successfully assign leave

*** Keywords ***
Background
    Given User Sudah Login
